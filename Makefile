update_submodule:
	git submodule update --remote --merge

run:
	go run cmd/main.go

create_proto_submodule:
	git submodule add git@gitlab.com:adds-uz/protos.git

proto-gen:
	./scripts/gen-proto.sh

swag:
	swag init -g api/router.go -o api/docs
