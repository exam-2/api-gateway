CREATE TABLE IF NOT EXISTS customers(
    id serial PRIMARY KEY,
    first_name TEXT NOT NULL,
    last_name   TEXT NOT NULL,
    bio     TEXT NOT NULL,
    email   TEXT NOT NULL,
    password    TEXT NOT NULL,
    phone TEXT NOT NULL,
    code TEXT NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    deleted_at TIMESTAMPTZ
);