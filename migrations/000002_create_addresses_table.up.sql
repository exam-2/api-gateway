CREATE TABLE IF NOT EXISTS addresses(
    customer_id INT NOT NULL REFERENCES customers(id),
    district TEXT NOT NULL,
    street TEXT NOT NULL,
    home_number TEXT NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    deleted_at TIMESTAMPTZ
);