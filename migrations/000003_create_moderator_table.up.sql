CREATE TABLE IF NOT EXISTS moderator(
    id SERIAL PRIMARY KEY, 
    name TEXT NOT NULL,
    email TEXT NOT NULL,
    password TEXT NOT NULL
);