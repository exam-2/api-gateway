FROM golang:1.19.3-alpine
RUN mkdir api-gateway
COPY . /api-gateway
WORKDIR /api-gateway
RUN go mod tidy
RUN go build -o main cmd/main.go
CMD ./main
EXPOSE 8080         