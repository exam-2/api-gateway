package services

import (
	"fmt"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"temp/config"
	ps "temp/genproto/customer"
	pp "temp/genproto/post"
	pr "temp/genproto/review"
)

type IServiceManager interface {
	CustomerService() ps.CustomerServiceClient
	PostService() pp.PostServiceClient
	ReviewService() pr.ReviewServiceClient
}

type serviceManager struct {
	customerService ps.CustomerServiceClient
	postService     pp.PostServiceClient
	reviewService   pr.ReviewServiceClient
}

func NewServiceManager(conf *config.Config) (IServiceManager, error) {
	connPost, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.PostServiceHost, conf.PostServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return &serviceManager{}, err
	}

	connCustomer, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.CustomerServiceHost, conf.CustomerServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return &serviceManager{}, err
	}
	connReview, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.ReviewServiceHost, conf.ReviewServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return &serviceManager{}, err
	}
	serviceManager := &serviceManager{
		postService:     pp.NewPostServiceClient(connPost),
		customerService: ps.NewCustomerServiceClient(connCustomer),
		reviewService:   pr.NewReviewServiceClient(connReview),
	}

	return serviceManager, nil
}

func (s *serviceManager) PostService() pp.PostServiceClient {
	return s.postService
}
func (s *serviceManager) CustomerService() ps.CustomerServiceClient {
	return s.customerService
}
func (s *serviceManager) ReviewService() pr.ReviewServiceClient {
	return s.reviewService
}
