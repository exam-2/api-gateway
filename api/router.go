package api

import (
	_ "temp/api/docs"
	v1 "temp/api/handlers/v1"
	"temp/api/middleware"
	"temp/api/token"
	"temp/config"
	"temp/pkg/logger"
	"temp/services"
	"temp/storage/repo"
	"github.com/gin-contrib/cors"
	"github.com/casbin/casbin/v2"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Redis          repo.InMemoryStorageI
	CasbinEnforcer *casbin.Enforcer
}



//	New ...
//	@title	exam api
//	@version 1.0
//	@description This is exam server api server
//	@termsOfService	http://swagger.io/terms/

//	@contact.name Mirobid

//	@host	localhost:9090
//	@BasePath	/v1

// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	jwtHandler := token.JWTHandler{
		SigninKey: option.Conf.SignInkey,
		Log:       option.Logger,
	}
	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
		Redis:          option.Redis,
	})

	corConfig := cors.DefaultConfig()
	corConfig.AllowAllOrigins = true
	corConfig.AllowCredentials = true
	corConfig.AllowHeaders = []string{"*"}
	corConfig.AllowBrowserExtensions = true
	corConfig.AllowMethods = []string{"*"}
	router.Use(cors.New(corConfig))

	router.Use(middleware.NewAuth(option.CasbinEnforcer, jwtHandler, config.Load()))

	api := router.Group("/v1")
	//Customer
	api.GET("/customer/getall/:id", handlerV1.GetAllCustomers)
	api.GET("/customer/get/:id", handlerV1.GetCustomers)
	api.DELETE("/customer/delete/:id", handlerV1.DeletedCustomer)
	api.PUT("/customer/updated", handlerV1.UpdateCustomer)
	api.POST("/customer/created", handlerV1.CreateCustomer)
	api.GET("/customer/listcustomer", handlerV1.ListCustomers)
	api.GET("/customer/login/:email/:password", handlerV1.Login)
	api.GET("/customer/search/order", handlerV1.GetCustomerBySearch)

	api.POST("/customer/register", handlerV1.RegisterCustomer)
	api.GET("/customer/verify/:email/:code", handlerV1.Verify)
	api.GET("/admin/login/:name/:password", handlerV1.GetAdminName)
	
	//Post
	api.POST("/post/created", handlerV1.CreatePost)
	api.PUT("/post/update", handlerV1.UpdatePost)
	api.DELETE("/post/delete/:id", handlerV1.DeletePost)
	api.GET("/post/get/:id", handlerV1.GetPosts)
	api.GET("/post/getall/:id", handlerV1.GetPostWithAll)
	api.GET("/post/getreview/:id", handlerV1.GetPostWithReview)
	api.GET("/post/search/order", handlerV1.GetPostBySearch)	

	//Review
	api.POST("/review/created", handlerV1.CreateReview)
	api.PUT("/review/update", handlerV1.UpdateReview)
	api.GET("/review/get/:id", handlerV1.GetReviewByPostID)

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return router
}
