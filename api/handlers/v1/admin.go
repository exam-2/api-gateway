package v1

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"temp/api/models"
	"temp/genproto/customer"
	"temp/pkg/etc"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)


// Login Admin
// @Summary 		Login Admin
// @Description 	this is Login...
// @Tags 			Login
// @Accept 			json
// @Produce 		json
// @Param name 		path string true "name"
// @Param password 	path string true "password"
// @Success 		200					  {object}  customer.GetAdminResp
// @Failure 		500					  {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Failure         404                   {object}  models.Error
// @Router /v1/admin/login/{name}/{password} [get]
func (h *handlerV1) GetAdminName(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true
	var (
		name     = c.Param("name")
		password = c.Param("password")
	)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	// fmt.Println(name)
	fmt.Println(customer.GetAminReq{Name: name})
	res, err := h.serviceManager.CustomerService().GetAdmin(ctx, &customer.GetAminReq{Name: name})
	fmt.Println(name, "<<<<--------api")
	fmt.Println(res, "<<<<<<<<--- res")
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusNotFound, models.Error{
			Error:       err,
			Description: "Mos keladigan ma'lumotni topa olmadingiz. Siz avval ro'yxatdan o'tganmisiz?",
		})
		h.log.Error("Error while getting with name")
		return
	}
	if !etc.CheckPasswordHash(password, res.Password) {
		c.JSON(http.StatusNotFound, models.Error{
			Error:       err,
			Description: "Password or name ERROR",
		})
		return
	}
	id := strconv.Itoa(int(res.Id))
	h.jwthandler.Iss = "admin"
	h.jwthandler.Sub = id
	h.jwthandler.Role = "admin"
	h.jwthandler.Aud = []string{"exam-app"}
	h.jwthandler.SigninKey = h.cfg.SignInkey
	h.jwthandler.Log = h.log
	tokens, err := h.jwthandler.GenerateAuthJWT()
	accessToken := tokens[0]
	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}
	res.AccessToken = accessToken
	res.Password = ""
	c.JSON(http.StatusOK, res)
}

// Login 		Moderator
// @Summary 	Login Moderator
// @Description this is Login...
// @Tags 		Login
// @Accept 		json
// @Produce 	json
// @Param 		name 	 path string true "name"
// @Param 		password path string ture "password"
// @Success 	200 {object} customer.GetModeratorResp
// @Failure 	500 {object} models.Error
// @Failure 	400 {object} models.Error
// @Failure 	404 {object} models.Error
// @Router /v1/moderator/login/{name}/{password} [get]
func (h *handlerV1) GetModerator(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true
	var (
		name     = c.Param("name")
		password = c.Param("password")
	)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	fmt.Println(name)
	res, err := h.serviceManager.CustomerService().GetModerator(ctx, &customer.GetModeratorReq{Name: name})
	fmt.Println(res.Name)
	if err != nil {
		c.JSON(http.StatusNotFound, models.Error{
			Error:       err,
			Description: "Mos keladigan ma'lumotni topa olmadingiz. Siz avval ro'yxatdan o'tganmisiz?",
		})
		h.log.Error("Error while getting with name")
		return
	}
	if !etc.CheckPasswordHash(password, res.Password) {
		c.JSON(http.StatusNotFound, models.Error{
			Error:       err,
			Description: "Password or name ERROR",
		})
		return
	}
	id := strconv.Itoa(int(res.Id))
	h.jwthandler.Iss = "moderator"
	h.jwthandler.Sub = id
	h.jwthandler.Role = "moderator"
	h.jwthandler.Aud = []string{"exam-app"}
	h.jwthandler.SigninKey = h.cfg.SignInkey
	h.jwthandler.Log = h.log
	tokens, err := h.jwthandler.GenerateAuthJWT()
	accessToken := tokens[0]
	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}
	res.AccessToken = accessToken
	res.Password = ""
	c.JSON(http.StatusOK, res)
}
