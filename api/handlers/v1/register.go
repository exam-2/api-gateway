package v1

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"temp/api/models"
	"temp/email"
	"temp/genproto/customer"
	"temp/pkg/etc"
	"temp/pkg/logger"
	"temp/pkg/utils"

	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"google.golang.org/protobuf/encoding/protojson"
)

//Register 		Customer
//@Summary		register Customer
//@Description 	this registers Customers
//@Tags 	Register
//@Accept 	json
//@Produce 	json
//@Param 	registerCustomer body models.CustomerReq true "Register"
//@Success 	200 "Code was sent to your email"
//@Failure	500 {object}	models.Error
//@Router	/v1/customer/register	[post]
func (h *handlerV1) RegisterCustomer(c *gin.Context) {
	var (
		body customer.CustomerReq
	)
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, models.Error{
			Error: err,
		})
		h.log.Error("Error while binding json", logger.Any("json", err))
		return
	}
	body.Email = strings.TrimSpace(body.Email)
	body.Email = strings.ToLower(body.Email)
	fmt.Println(body.Password, "<- body.Password")
	body.Password, err = etc.HashPassword(body.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error: err,
		})
		h.log.Error("couldn`t hash the password")
		return
	}
	fmt.Println(body.Email, "<-BODY")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	fmt.Println(`""""`, ctx)

	emailExists, err := h.serviceManager.CustomerService().CheckField(ctx, &customer.CheckFieldReq{
		Field:  "email",
		Values: body.Email,
	})

	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, models.Error{
			Error: err,
			Description: "Email already used",
		})
		h.log.Error("Error while cheking email uniqeness", logger.Any("check", err))
		return
	}

	if emailExists.Exists {
		c.JSON(http.StatusConflict, models.Error{
			Error:       err,
			Description: "You have already signed in",
		})
		return
	}

	exists, err := h.redis.Exists(body.Email)
	if err != nil {
		h.log.Error("Error while checking email uniqueness")
		c.JSON(http.StatusConflict, models.Error{
			Error: err,
		})
		return
	}

	if cast.ToInt(exists) == 1 {
		fmt.Println("register.go 97")
		c.JSON(http.StatusConflict, models.Error{
			Error: err,
		})
		return
	}

	customerToBeSaved := &customer.CustomerReq{
		Id:          time.Now().UTC().Unix(),
		FirstName:   body.FirstName,
		LastName:    body.LastName,
		Email:       body.Email,
		Wikipedia:   body.Wikipedia,
		PhoneNumber: body.PhoneNumber,
		Password:    body.Password,
		Code: body.Code,
	}
	for _, address := range body.Address {
		customerToBeSaved.Address = append(customerToBeSaved.Address, &customer.Address{
			District:   address.District,
			Street:     address.Street,
			HomeNumber: address.HomeNumber,
		},
		)
	}
	customerToBeSaved.Code = etc.GenerateCode(6)
	msg := "Subject: Exam email verification\n Your verification code:" + customerToBeSaved.Code
	err = email.SendEmail([]string{body.Email}, []byte(msg))

	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error: err,

			Description: "Your Email is not valid, Please recheck it",
		})
		return
	}
	c.JSON(http.StatusAccepted, models.Error{
		Error:       nil,
		Description: "Your request successfuly accepted we have send code to your email, Your code is : " + customerToBeSaved.Code,
	}) // Code should be sent to gmail

	bodyByte, err := json.Marshal(customerToBeSaved)
	if err != nil {
		h.log.Error("Error while marshaling to json", logger.Any("json", err))
		return
	}
	err = h.redis.SetWithTTL(customerToBeSaved.Email, string(bodyByte), 300)
	if err != nil {
		h.log.Error("Error while marshaling to json", logger.Any("json", err))
		return
	}
}

//Verify C		ustomer
//@Summary 		Verify Customer
//@Description	Verify Customer
//@Tags 		Verify
//@Accept 		json
//@Produce 		json
//@Param		email 	path 		string true "email"
//@Param    	code   	path 		string true "code"
//@Success		200 	{object}	models.VerifyResponse
//@Router	/v1/customer/verify/{email}/{code}	[get]
func (h *handlerV1) Verify(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	var (
		code  = c.Param("code")
		email = c.Param("email")
	)
	customerBody, err := h.redis.Get(email)
	if err != nil {
		c.JSON(http.StatusGatewayTimeout, gin.H{
			"info":  "Your time has expired",
			"error": err.Error(),
		})
		h.log.Error("Error while getting customer from redis", logger.Any("redis", err))
	}
	customerBodys := cast.ToString(customerBody)
	fmt.Println(customerBody, "<-")
	body := customer.CustomerReq{}
	err = json.Unmarshal([]byte(customerBodys), &body)
	
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while unmarshaling from json to customer body", logger.Any("json", err))
		return
	}
	if body.Code != code {
		fmt.Println(body.Code)
		c.JSON(http.StatusConflict, gin.H{
			"info": "Wrong code",
		})
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	res, err := h.serviceManager.CustomerService().CreateCustomer(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while creating customer", logger.Any("post", err))
		return
	}
	id := strconv.Itoa(int(res.Id))
	responce := &models.VerifyResponse{
		Id:          id,
		FirstName:   res.FirstName,
		LastName:    res.LastName,
		Email:       res.Email,
		Bio:         res.Wikipedia,
		PhoneNumber: res.PhoneNumber,
	}
	for _, add := range res.Address {
		adres_id := strconv.Itoa(int(add.CustomerId))
		responce.Addresses = append(responce.Addresses, models.AddressResponse{
			Id:         adres_id,
			District:   add.District,
			Street:     add.Street,
			HomeNumber: add.HomeNumber,
		})
	}

	h.jwthandler.Iss = res.FirstName
	h.jwthandler.Sub = id
	h.jwthandler.Role = "authorized"
	h.jwthandler.Aud = []string{"exam-app"}
	h.jwthandler.SigninKey = h.cfg.SignInkey
	tokens, err := h.jwthandler.GenerateAuthJWT()
	accessToken := tokens[0]
	refreshToken := tokens[1]

	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}
	responce.JWT = accessToken
	responce.RefreshToken = refreshToken

	c.JSON(http.StatusOK, responce)
}

//Login Customer
//@Summary Login Customer
//@Description this is Login...
//@Tags 	Login
//@Accept 	json
//@Produc1e 	json
//@Param email 		path string true "email"
//@Param password 	path string true "password"
//@Success 		   	200 				  {object}  models.GetLoginResp
//@Failure 		   	500					  {object}  models.Error
//@Failure        	400					  {object}  models.Error
//@Failure         	404                   {object}  models.Error
//@Router /v1/customer/login/{email}/{password} [get]
func (h *handlerV1) Login(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true
	var (
		email    = c.Param("email")
		password = c.Param("password")
	)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.CustomerService().GetEmail(ctx, &customer.LoginReq{
		Email: email,
	})
	fmt.Print(email) 
	if err != nil {
		c.JSON(http.StatusNotFound, models.Error{
			Error:       err,
			Description: "Mos keladigan ma'lumotni topa olmadingiz. Siz avval ro'yxatdan o'tganmisiz?",
		})
		h.log.Error("Error while getting with email")
		return
	}

	id := strconv.Itoa(int(res.Id))
	responce := &models.GetLoginResp{
		Id: id,
		FirstName: res.FirstName,
		LastName: res.LastName,
		Email: res.Email,
		Bio: res.Wikipedia,
		PhoneNumber: res.PhoneNumber,
	}
	for _, add := range responce.Addresses {
	
		responce.Addresses = append(responce.Addresses, models.Address{
			Id: add.Id,
			District: add.District,
			Street: add.Street,
			HomeNumber: add.HomeNumber,
		})
	}

	fmt.Println(err, "ERRRRR", password, res.Password)
	if !etc.CheckPasswordHash(password, res.Password) {
		c.JSON(http.StatusNotFound, models.Error{
			Error:       err,
			Description: "Password or email ERROR",
		})
		return
	}
	// responce := &models.UpdateAccessToken{}
	h.jwthandler.Iss = res.FirstName
	h.jwthandler.Sub = id
	h.jwthandler.Role = "authorized"
	h.jwthandler.Aud = []string{"exam-app"}
	h.jwthandler.SigninKey = h.cfg.SignInkey
	tokens, err := h.jwthandler.GenerateAuthJWT()
	accessToken := tokens[0]
	refreshToken := tokens[0]
	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}
	responce.JWT = accessToken
	responce.RefreshToken = refreshToken

	res.Password = ""
	c.JSON(http.StatusOK, responce)
}

//Get Post list by search and order by
//@Summary Get Customer by search and order
//@Description Get customer Get Customer list by order and search:
// @Description     Search fields -> title, description, content 
// @Description     ORDER fields -> field:DESC or field:ASC 
// @Tags 			Customer
// @Security        BearerAuth
// @Accept 			json
// @Produce         json
// @Param           page        query     int	    true "Page"
// @Param           limit       query     int	    true "limit"
// @Param           search      query     string    true "Search format should be 'key:value'"
// @Param           order       query     string    true "order format should be 'key:value'"
// @Success         200					  {object} 	customer.ListCustomersResp
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
//@Router	/v1/customer/search/order [get]
func (h *handlerV1) GetCustomerBySearch(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil{
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return

	// }
	// if claims.Role != "Authorized"{
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }
	queryParams := c.Request.URL.Query()
	search := strings.Split(c.Query("search"), ":")
	order := strings.Split(c.Query("order"), ":")
	fmt.Println(search, order)
	if len(search) != 2 && len(order) != 2{
		c.JSON(http.StatusBadRequest, models.Error{
			Description: "Enter needed params",
		})
		h.log.Error("failed to get all params")
		return
	}
	param, errSS := utils.ParseQueryParams(queryParams)
	if errSS != nil{
		c.JSON(http.StatusBadRequest, models.Error{
			Description: errSS[0],
		})
		h.log.Error("failed to parse query params" + errSS[0])
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.CustomerService().GetCustomerBySearch(ctx, &customer.GetListCustomerSearch{
		Limit:	param.Limit,
		Page:  param.Page,
		Search: &customer.SearchFields{
			Field: search[0],
			Value: search[1],
		},
		Orders: &customer.Order{
			Field: order[0],
			Value: order[1],
		}})
	if err != nil{
		c.JSON(http.StatusInternalServerError, models.Error{
			Description: "couldn't Get",
		})
		h.log.Error("Get .Post().GetPostBySearchAndOrder(ctx, &ps.GetListBySearchReq{", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK,res)
} 