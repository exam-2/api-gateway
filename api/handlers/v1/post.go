package v1

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	_ "temp/api/docs"
	"temp/api/models"
	"temp/genproto/post"
	"temp/pkg/logger"
	"temp/pkg/utils"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

//Create
//@Summary create post by api
//@Description create new posts
//@Tags Post
//@Security        BearerAuth
//@Accept json
//@Produce json
//@Param post body post.PostReq true "Post"
//@Success 200 {object} post.PostResp
//@Router /v1/post/created [post]
func (h *handlerV1) CreatePost(c *gin.Context) {
	var (
		body 	post.PostReq
		jspbMarshal protojson.MarshalOptions
	)
	fmt.Println("Hello World")
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil{
		fmt.Println(`ErrorS  `, err)
		c.JSON(http.StatusBadRequest, gin.H{
			"error":err.Error(),
		})
		h.log.Error("Failed bind JSON",logger.Error(err))
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	posts := &post.PostReq{
		CustomerId: body.CustomerId,
		Name: body.Name,
		Description: body.Description,
		About: body.About,
	}
	for _, media := range body.Media{
		posts.Media = append(posts.Media, &post.Media{
			Name: media.Name,
			Link: media.Link,
			Type: media.Link,
		})
	}
	responce, err := h.serviceManager.PostService().CreatePost(ctx, &body)
	if err != nil{
		fmt.Println(`Error This  `, err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error":err.Error(),
		})
		h.log.Error("Error while inserting post",logger.Error(err))
		return
	}
c.JSON(http.StatusCreated, responce)
}

//Update
//@Summary update post by api
//@Description updated new post
//@Tags Post
// @Security        BearerAuth
//@Accept json
//@Produce json
//@Param post body post.PostResp true "Post"
//@Success 200 {object} post.PostResp
//@Router /v1/post/update [put]
func (h *handlerV1) UpdatePost(c *gin.Context) {
	var (
		body post.PostResp
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil{
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Failed to bind json", logger.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(),time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	responce, err := h.serviceManager.PostService().UpdatePost(ctx, &body)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		h.log.Error("Failed to Update user", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, responce)
}

//Delete
//@Summary deleted post by api
//@Description deleted by POSTID
//@Tags Post
// @Security        BearerAuth
//@Accept json
//@Produce json
//@Param id path int true "Post"
//@Success 200 {object} post.Empty
//@Router /v1/post/delete/{id} [delete]
func (h *handlerV1) DeletePost(c *gin.Context) {
	var (
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(),time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	responce, err := h.serviceManager.PostService().DeletePost(ctx, &post.PostID{PostId: id})
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		h.log.Error("Error while deleting user", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, responce)
}

//GetPost
//@Summary getting post by api
//@Description getting by POSTID
//@Tags Post
// @Security        BearerAuth
//@Accept json
//@Produce json
//@Param id path int true "Post"
//@Success 200 {object} post.PostResp
//@Router /v1/post/get/{id} [get]
func (h *handlerV1) GetPosts(c *gin.Context) {
	var (
		body post.PostID
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", logger.Error(err))
		return
	}
	body.PostId = id
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	responce, err := h.serviceManager.PostService().GetPosts(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get user info", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, responce)
}

//GetAllPosts
//@Summary getting post by api
//@Description getting by POSTID
//@Tags Post
// @Security        BearerAuth
//@Accept json
//@Produce json
//@Param id path int true "Post"
//@Success 200 {object} post.PostRespAllinfo
//@Router /v1/post/getall/{id} [get]
func (h *handlerV1) GetPostWithAll(c *gin.Context) {
	var (
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	fmt.Println("Click THIS IS    ")
	responce, err := h.serviceManager.PostService().GetPostWithAll(ctx, &post.PostID{PostId: id})
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get user info", logger.Error(err))
		return
	}
	fmt.Println(`Successs GetPostALL`)	
	c.JSON(http.StatusOK, responce)
}

//GetPostWithReview
//@Summary getting all post by api
//@Description getting by CustomerID
//@Tags Post
//@Security        BearerAuth
//@Accept json
//@Produce json
//@Param id path int true "Post"
//@Success 200 {object} post.PostswithReview
//@Router /v1/post/getreview/{id} [get]
func (h *handlerV1) GetPostWithReview(c *gin.Context) {
	var (
		body	post.PostID
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", logger.Error(err))
		return
	}
	body.PostId = id
	
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	responce, err := h.serviceManager.PostService().GetPostWithReview(ctx, &body)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get user info", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK,responce)
}

//GetPostSearch
//@Summary Get post by search and order
//@Description Get post list by SEARCH and ORDER
//@Description Order Fields >>> FIELD:DESC or FIELD:ASC
//@Description Search Fields >>> name, description, about
//@Tags Post
//@Security        BearerAuth
//@Accept json
//@Produce json
//@Param page query int true "PAGE"
//@Param limit query int true "LIMIT"
//@Param search query string 	true "Search format should be 'key:value'"
//@Param order query string 	true "Order format should be 'key:value'"
//@Success  200 {object}	post.ListPostResp
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
//@Router /v1/post/search/order	[get]
func (h *handlerV1) GetPostBySearch(c *gin.Context) {
	queryParams := c.Request.URL.Query()
	search := strings.Split(c.Query("search"),`:`)
	order  := strings.Split(c.Query("order") ,`:`)
	fmt.Print(search, order)
	if len(search) != 2 && len(order) != 2{
		c.JSON(http.StatusBadRequest, models.Error{
			Description: "Enter needed params",
		})
		h.log.Error(`failed to get all params`)
		return
	}
	param, errSS := utils.ParseQueryParams(queryParams)
	if errSS != nil{
		c.JSON(http.StatusBadRequest, models.Error{
			Description: errSS[0],
		})
		h.log.Error(`failed to parse query params`)
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.PostService().GetPostBySearch(ctx, &post.GetPostSearchReq{
		Limit: param.Limit,
		Page: param.Page,
		Search: &post.SearchField{
			Field: search[0],
			Value: search[1],
		},
		Orders: &post.Order{
			Field: order[0],
			Value: order[1],
		}})
	
	if err != nil{
		c.JSON(http.StatusInternalServerError, models.Error{
			Description: `couldnt Get`,
		})
		h.log.Error(`failed provaling with limit and page`)
		return
	}
	c.JSON(http.StatusOK, res)
}