package v1

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	_ "temp/api/docs"
	"temp/genproto/review"
	"temp/pkg/logger"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

//Create
//@Summary create reviews by api
//@Description create new review
//@Tags Review
//@Security BearerAuth
//@Accept json
//@Produce json
//@Param review body review.ReviewReq true "Review"
//@Success 200 {object} review.ReviewResp
//@Router /v1/review/created [post]
func (h *handlerV1) CreateReview(c *gin.Context) {
	var (
		body	*review.ReviewReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil{
		c.JSON(http.StatusBadRequest, gin.H{
			"error":err.Error(),
		})
		h.log.Error("Failed bind JSON",logger.Error(err))
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	responce, err := h.serviceManager.ReviewService().CreateReview(ctx, body) 
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error":err.Error(),
		})
		h.log.Error("Failed THE bind JSON",logger.Error(err))
		return
	}
	c.JSON(http.StatusCreated, responce)
}

//Update
//@Summary update reviews by api
//@Description update review by ID
//@Tags Review
//@Security BearerAuth
//@Accept json
//@Produce json
//@Param review body review.ReviewResp true "Review"
//@Success 200 {object} review.ReviewResp
//@Router /v1/review/update [put]
func (h *handlerV1) UpdateReview(c *gin.Context) {
	var (
		body review.ReviewResp
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)
	if err != nil{
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Failed to bind json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	responce, err := h.serviceManager.ReviewService().UpdateReview(ctx, &body)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"Error": err.Error(),
		})
		h.log.Error("Failed to Update user", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, responce)
}

//GetReviewsByPOSTID
//@Summary getting reviews by api
//@Description delete review by ID
//@Tags Review
//@Security BearerAuth
//@Accept json
//@Produce json
//@Param id path int true "GetReviewByPostID"
//@Success 200 {object} review.ReviewList
//@Router /v1/review/get/{id} [get]
func (h *handlerV1) GetReviewByPostID(c *gin.Context) {
	fmt.Println(`Hello World`)
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true
	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	fmt.Println(id)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	responce, err := h.serviceManager.ReviewService().GetReviewByPostID(ctx, &review.ReviewID{ReviewID: id})
	fmt.Println(`Responce`, responce)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get user info", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, responce)
}