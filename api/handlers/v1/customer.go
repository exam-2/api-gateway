package v1

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	_ "temp/api/docs"
	"temp/api/models"

	// "temp/api/models"
	"temp/genproto/customer"
	"temp/pkg/logger"
	"temp/pkg/utils"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

//Create
//@Summary 		Add new  CustomerInfo
//@Description 	Through this api, CustomerInfo can be added
//@Tags 		Customer
//@Security     BearerAuth
//@Accept 		json
//@Produce 		json
//@Param 		AddCustomer 	body 	models.CustomerReq true "CustomerReq"
//@Success		   201 		{object}	customer.CustomerResp
//@Failure         500      {object}  	models.ErrorInfo
//@Failure         400      {object}  	models.ErrorInfo
//@Router		/v1/customer/created	[post]
func (h *handlerV1) CreateCustomer(c *gin.Context) {
	_, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorInfo{
			Code: http.StatusInternalServerError,
			Message: "Invalid access Token",
			Error: err,
		})
		h.log.Error("Error while getting claims of access token", logger.Error(err))
		return
	}

	body := &models.CustomerReq{}
	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ErrorInfo{
			Code: http.StatusBadRequest,
			Message:	"Enter Right Info",
			Error: err,
		})
		h.log.Error("Error while marshaling from request", logger.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	responce := &customer.CustomerReq{
		FirstName: body.FirstName,
		LastName: body.LastName,
		Wikipedia: body.Bio,
		Email: body.Email,
		Password: body.Password,
		PhoneNumber: body.PhoneNumber,
	}
	for _, add := range responce.Address {
		responce.Address = append(responce.Address, &customer.Address{
			CustomerId: responce.Id,
			District: add.District,
			Street: add.Street,
			HomeNumber: add.HomeNumber,
		})
	}

	res, err := h.serviceManager.CustomerService().CreateCustomer(ctx, responce)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorInfo{
			Code: http.StatusInternalServerError,
			Message:	"Something went wrong",
			Error: err,
		})
		h.log.Error("Error while Updating", logger.Error(err))
		return
	}

	c.JSON(http.StatusCreated, res)
}

//Update
//@Summary 			updated Customer by api
//@Description 		updated new Customer about
//@Tags 		   	Customer
//@Security        	BearerAuth
//@Accept 	json
//@Produce 	json
//@Param 	UpdateCustomer 		 body 	  models.CustomerResp true "Customer"
//@Success 		   200		{object}  customer.CustomerResp
//@Failure         500      {object}  models.ErrorInfo
//@Failure         400      {object}  models.ErrorInfo
//@Router /v1/customer/updated [put]CustomerResp
func (h *handlerV1) UpdateCustomer(c *gin.Context) {
	 var (
		body 		models.CustomerResp
		jspbMarshal protojson.MarshalOptions 
	 )
	 jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil{
		c.JSON(http.StatusBadRequest, models.ErrorInfo{
			Code: http.StatusBadRequest,
			Message: "Enter Right Info",
			Error: err,
		})
		h.log.Error("Failed to bind json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	responce, err := h.serviceManager.CustomerService().UpdateCustomer(ctx, &customer.CustomerResp{
		Id: int64(body.Id),
		FirstName: body.FirstName,
		LastName: body.LastName,
		Wikipedia: body.Bio,
		Email: body.Email,
		PhoneNumber: body.PhoneNumber,
		Password: body.Password,
		Code: body.Code,
	})
	if err != nil{
		c.JSON(http.StatusInternalServerError, models.ErrorInfo{
			Code: http.StatusInternalServerError,
			Message: "Something went wrong",
			Error: err,
		})
		h.log.Error("Failed to Update user", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, responce)
}

//GetCustomer
//@Summary 		getting 	Customer by api
//@Description 	getting 	Customer by id
//@Tags 		Customer
//@Security     BearerAuth
//@Accept 		json
//@Produce 		json
//@Param 		id 		path 		int 	true "Get"
//@Success 		200 	{object} 	customer.CustomerResp
//@Failure      500     {object}  	models.Error
//@Failure      400     {object}  	models.Error	
//@Router /v1/customer/get/{id} [get]
func (h *handlerV1) GetCustomers(c *gin.Context) {
	// claims, err := GetClaims(*h, c)
	// if err != nil{		
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	h.log.Error("Checking Authorization", logger.Error(err))
	// 	return
	// }
	// if claims.Role != "authorized"{
	// 	c.JSON(http.StatusUnauthorized, models.Error{
	// 		Error:       err,
	// 		Description: "You are not authorized",
	// 	})
	// 	return
	// }
	var (
		body customer.CustomerID
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", logger.Error(err))
		return
	}
	body.Id = id
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	responce, err := h.serviceManager.CustomerService().GetCustomers(ctx, &body)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get user info", logger.Error(err))
		return
	}
	res := models.CustomerResp{
		FirstName: responce.FirstName,
		LastName: responce.LastName,
		Bio: responce.Wikipedia,
		Email: responce.Email,
		Password: responce.Password,
		PhoneNumber: responce.PhoneNumber,
		Code: responce.Code,
	}
	for _, add := range res.Addresses {
		res.Addresses = append(res.Addresses, models.Address{
			Id: res.Id,
			District: add.District,
			Street: add.Street,
			HomeNumber: add.HomeNumber,
		})
	}
	
	c.JSON(http.StatusOK, responce)
}

//GetAllCustomer
//@Summary 		getting Customer by api
//@Description 	getting Customer by ID
//@Tags 		Customer
//@Security     BearerAuth
//@Accept 		json
//@Produce 		json
//@Param 		id 		path	  int true "GetALL"
//@Success 		200 	{object}  customer.GetCustomer
//@Failure      500     {object}  models.Error
//@Failure      400     {object}  models.Error
//@Router /v1/customer/getall/{id} [get]
func (h *handlerV1) GetAllCustomers(c *gin.Context) {
	var (
		body	customer.CustomerID
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames =true
	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", logger.Error(err))
		return
	}
	body.Id = id
	
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	fmt.Println("This ErrorRRRRRRRR			")
	responce, err := h.serviceManager.CustomerService().GetAllCustomers(ctx, &body)
	if err != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get user info", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, responce)
}

//DeleteCustomer
//@Summary 		deleted Customer by api
//@Description 	Through this api, customerInfo can Deleted
//@Tags 		Customer
//@Security     BearerAuth
//@Accept 		json
//@Produce 		json
//@Param 		id 	path 	 	int true "Delete"
//@Success 		200 {object} 	models.SuccessInfo
//@Failure		500	{object} 	models.ErrorInfo
//@Failure		400 {object}	models.ErrorInfo
//@Router 		/v1/customer/delete/{id} [delete]
func (h *handlerV1) DeletedCustomer(c *gin.Context) {

	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil{
		c.JSON(http.StatusInternalServerError, models.ErrorInfo{
			Code: http.StatusBadRequest,
			Message: "Enter Right Info",
			Error: err,
		})
		h.log.Error("failed parse string to int", logger.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	_, err = h.serviceManager.CustomerService().DeletedCustomer(ctx, &customer.CustomerID{Id: id})
	if err != nil{
		c.JSON(http.StatusInternalServerError, models.ErrorInfo{
			Code: http.StatusInternalServerError,
			Message: "Something went wrong",
			Error: err,
		})
		h.log.Error("failed to get user info", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, models.SuccessInfo{
		Message: "Customer Successfuly deleted",
		StatusCode: http.StatusOK,
	})
} 

//ListCustomers
//@Summary getting limit page by api
//@Description getting Customer by limit and page
//@Tags Customer
//@Security        BearerAuth
//@Accept json
//@Produce json
//@Param limit query string true "Limit"
//@Param page query string true "Page"
//@Success 200 {object} customer.ListCustomersResp
//@Router /v1/customer/listcustomer	[get]
func (h *handlerV1) ListCustomers(c *gin.Context) {
	queryParams := c.Request.URL.Query()
	params, errString := utils.ParseQueryParams(queryParams)
	if errString != nil{
		c.JSON(http.StatusBadRequest, gin.H{
			"error": errString,
		})
		h.log.Error("failed to get customer info")
		return 
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	responce, errs := h.serviceManager.CustomerService().ListCustomers(ctx, &customer.ListCustomersReq{
		Limit: params.Limit,
		Page: params.Page,
	})
	if errs != nil{
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": errs,
		})
		h.log.Error("Error with responce")
		return
	}
	c.JSON(http.StatusOK, responce)
}