package models

type Error struct {
	Error       error
	Description string
}
type VerifyResponse struct {
	Id           string            `json:"id"`
	FirstName    string            `json:"first_name"`
	LastName     string            `json:"last_name"`
	Email        string            `json:"email"`
	Bio          string            `json:"bio"`
	PhoneNumber  string            `json:"phone_number"`
	JWT          string            `json:"jwt"`
	RefreshToken string            `json:"refresh"`
	Addresses    []AddressResponse `json:"addresses"`
}
type AddressResponse struct {
	Id         string `json:"id"`
	OwnerId    string `json:"owner_id"`
	District   string `json:"district"`
	Street     string `json:"street"`
	HomeNumber string `json:"home_number"`
}

type Address struct {
	Id         int    `json:"id"`
	District   string `json:"district"`
	Street     string `json:"street"`
	HomeNumber string `json:"home_number"`
}

type CustomerReq struct {
	FirstName   string    `json:"first_name"`
	LastName    string    `json:"last_name"`
	Email       string    `json:"email"`
	Bio         string    `json:"bio"`
	Password    string    `json:"password"`
	PhoneNumber string    `json:"phone_number"`
	Addresses   []Address `json:"addresses"`
}

type CustomerResp struct {
	Id          int       `json:"id"`
	FirstName   string    `json:"first_name"`
	LastName    string    `json:"last_name"`
	Bio         string    `json:"bio"`
	Email       string    `json:"email"`
	Password    string    `json:"password"`
	PhoneNumber string    `json:"phone_number"`
	Code        string    `json:"code"`
	Addresses   []Address `json:"addresses"`
}

type GetLoginResp struct {
	Id           string    `json:"id"`
	FirstName    string    `json:"first_name"`
	LastName     string    `json:"last_name"`
	Email        string    `json:"email"`
	Bio          string    `json:"bio"`
	PhoneNumber  string    `json:"phone_number"`
	JWT          string    `json:"jwt"`
	RefreshToken string    `json:"refresh"`
	Addresses    []Address `json:"addresses"`
}
type GetAllCustomerResp struct {
	
}